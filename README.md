## Namaste 🙏🏻, I'm Mayank! 👋

💻 Senior Software Engineer @ IDfy experienced with Python3, Javascript, Elixir, currently enjoying experimenting with React.js.

🦊 GitLab open source contributor since July 2023.

🇮🇳 Recently on the Video Soltutions & Know your customer team at IDfy building features around Background Verification & Digital KYC - [IDfy](https://www.idfy.com/inbox)
